<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Рубрикатор</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</head>
<body>
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-md-3">
            <h4>Рубрики</h4>
            <ul class="list-group">
                @foreach($listCategories[0] ?? collect([])->sortBy('sort_order') as $cat)
                    @include('admin.partials.local_category', ['category' => $cat])
                @endforeach
            </ul>
        </div>
        <div class="col-md-2">
            <h4>Файл фида</h4>
            <ul class="list-group">
                @foreach($feedCategories as $feed => $categories)
                    <li class="list-group-item" data-open-feed="{{ $feed }}"
                        data-target-feed="feed-{{ $feed }}">
                        {{ __('admin.feeds.' . $feed) }}
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-7">
            <h4>Привязка рубрик</h4>
            <div class="d-flex">
                @foreach($feedCategories as $feed => $categories)
                    @include('admin.partials.categories_list', [
                        'feed' => $feed,
                        'categories' => $categories,
                        'localCategories' => $localCategories,
                        ])
                @endforeach
            </div>

            <select style="display:none;" id="local-categories-list">
                @foreach($localCategories as $locCat)
                    <option value="{{ $locCat->category_id }}"
                            data-short="{{ $locCat->description->name ?? '?' }}">
                        {{ implode(' <- ', $locCat->path) }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

@include('admin.partials.categories_scripts')

</body>
</html>

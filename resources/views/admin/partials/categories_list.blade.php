<div class="feed" data-category-feed="{{ $feed }}" id="feed-{{ $feed }}" style="display:none;">
    @for($i = 0; $i < 3; $i++)
        <div class="d-flex mr-3" data-row="{{ $i }}">
            @foreach($categories as $parent_id => $items)
                @if($i == 0 && $parent_id != null)
                    @continue
                @endif
                <ul class="list-group"
                    @if($i != 0) style="display:none;" @endif
                    data-feed="{{ $feed }}" data-row="{{ $i }}"
                    id="row-{{ $i }}-{{ $feed }}-{{ $parent_id }}">
                    @foreach($items as $category)
                        <li class="list-group-item"
                            data-feed="{{ $feed }}" data-id="{{ $category->id }}"
                            data-target="row-{{ $i + 1 }}-{{ $feed }}-{{ $category->id }}"
                            data-row="{{ $i }}">
                            {{ $category->feed_name }}
                            @if(!isset($categories[$category->id]))
                                <div class="mt-1">
                                    <small class="text-secondary cat_name" data-cs
                                           @if($category->category == null)
                                           style="display:none;"
                                           @else
                                           style="display:block;"
                                        @endif
                                    >
                                        {{ $category->category->description->name ?? '' }}
                                    </small>

                                    <small class="text-danger" data-ns
                                           @if($category->category != null)
                                           style="display:none;"
                                           @else
                                           style="display:block;"
                                        @endif
                                    >
                                        Категория не задана
                                    </small>

                                    <div class="mt-1">
                                        <div data-cs
                                             @if($category->category == null)
                                             style="display:none;"
                                            @endif
                                        >
                                            <small class="link-primary"
                                                   data-val="{{ $category->category_id }}"
                                                   data-change="{{ $category->id }}">
                                                Изменить
                                            </small>
                                            <small class="link-danger"
                                                   data-remove="{{ $category->id }}">
                                                Удалить
                                            </small>
                                        </div>
                                        <div data-ns
                                             @if($category->category != null)
                                             style="display:none;"
                                            @endif
                                        >
                                            <small class="link-primary"
                                                   data-val="{{ $category->category_id }}"
                                                   data-change="{{ $category->id }}">
                                                Задать категорию
                                            </small>
                                        </div>
                                        <div class="mt-1 cat-edit" style="display:none;">
                                            <select></select>
                                            <button class="btn btn-sm btn-primary" btn-save>
                                                Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endforeach
        </div>
    @endfor
</div>

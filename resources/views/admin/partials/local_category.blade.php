<li class="list-group-item">
    @isset($listCategories[$category->category_id])
        <a data-bs-toggle="collapse" href="#cat-{{ $category->category_id }}" role="button"
           aria-expanded="false" aria-controls="cat-{{ $category->category_id }}">
            {{ $category->description->name ?? '?' }}
        </a>
    @else
        {{ $category->description->name ?? '?' }}
    @endisset

    @isset($listCategories[$category->category_id])
        <ul class="collapse list-group" id="cat-{{ $category->category_id }}">
            @foreach($listCategories[$category->category_id]->sortBy('sort_order') as $subCat)
                @include('admin.partials.local_category', ['category' => $subCat])
            @endforeach
        </ul>
    @endisset
</li>

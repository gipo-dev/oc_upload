<script>
    $(function () {

        $('[data-open-feed]').click(function () {

            $('.feed').hide();
            $('#' + $(this).data('target-feed')).show();

            $('[data-open-feed]').removeClass('active');
            $(this).addClass('active');

        });


        $('[data-target]').click(function () {

            var row = $(this).data('row');

            $('.list-group[data-row]').filter(function () {
                return $(this).data('row') > row;
            }).hide();
            $('#' + $(this).data('target')).show();

            $('.list-group-item[data-row=' + (row) + ']').removeClass('active');

            if ($(this).find('[data-change]').length == 0)
                $(this).addClass('active');

        });

        $('[data-remove]').click(function () {
            var that = $(this);
            $.ajax({
                url: '{{ route('admin.categories.remove') }}',
                data: {
                    id: that.data('remove'),
                },
            });

            var item = that.parents('li');
            item.find('[data-cs]').hide();
            item.find('[data-ns]').show();
        });

        $('[data-change]').click(function () {
            var item = $(this).parents('li');
            item.find('.cat-edit').show();
            item.find('[data-cs], [data-ns]').hide();

            item.find('select').html(
                $('#local-categories-list').html()
            ).val(
                $(this).data('val')
            ).select2();
        });

        $('[btn-save]').click(function () {
            var that = $(this);
            var item = that.parents('li');
            var val = item.find('select').val();

            $.ajax({
                url: '{{ route('admin.categories.set') }}',
                data: {
                    id: item.find('[data-change]').data('change'),
                    value: val,
                },
            });

            item.find('.cat_name').text(
                $('#local-categories-list option[value="' + val + '"]').data('short')
            );
            item.find('[data-cs]').show();
            item.find('.cat-edit').hide();
            item.find('[data-change]').data('val', val);
        });

        $('[select2]').select2();

    });
</script>


<style>
    .feed {
        display: flex;
        flex-direction: row;
    }

    [data-row] {
        display: flex;
        flex-direction: column;
    }

    .mr-3 {
        margin-right: 10px;
    }

    [data-remove], [data-change] {
        cursor: pointer;
    }

    .select2-container {
        width: 270px !important;
    }

    .select2-selection, .select2-results__option {
        font-size: 14px;
    }

    [data-target-feed], [data-target] {
        cursor: pointer;
    }
</style>

<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_attribute_group';

    /**
     * @var string
     */
    protected $primaryKey = 'attribute_group_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'sort_order' => 0,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function description()
    {
        return $this->hasOne(AttributeGroupDescription::class,
            'attribute_group_id', 'attribute_group_id');
    }

    public function attrs() {
        return $this->hasMany(Attribute::class, 'attribute_group_id', 'attribute_group_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_product';

    /**
     * @var string
     */
    protected $primaryKey = 'product_id';

    /**
     * @var array
     */
    protected $guarded = [];

    protected $attributes = [
        'sku' => '',
        'upc' => '',
        'ean' => '',
        'jan' => '',
        'isbn' => '',
        'mpn' => '',
        'location' => '',
        'quantity' => 1000,
        'stock_status_id' => 7,
        'manufacturer_id' => 0,
        'shipping' => 1,
        'cost' => 0,
        'points' => 1,
        'tax_class_id' => 0,
        'weight' => 0,
        'weight_class_id' => 1,
        'length' => 0,
        'width' => 0,
        'height' => 0,
        'length_class_id' => 1,
        'subtract' => 0,
        'minimum' => 1,
        'sort_order' => 1,
        'status' => 1,
        'viewed' => 0,
        'noindex' => 0,
        'dn_id' => 0,
        'import_batch' => 1,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images() {
        return $this->hasMany(ProductImage::class,
            'product_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function description()
    {
        return $this->hasOne(ProductDescription::class,
            'product_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attrs()
    {
        return $this->hasMany(ProductAttribute::class,
            'product_id', 'product_id');
    }
}

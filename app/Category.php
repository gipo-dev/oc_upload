<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_category';

    /**
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'top' => 1,
        'column' => 1,
        'sort_order' => 0,
        'status' => 1,
        'noindex' => 0,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function description()
    {
        return $this->hasOne(CategoryDescription::class,
            'category_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'category_id');
    }

    /**
     * @return string[]
     */
    public function getPathAttribute()
    {
        $path = [$this->description->name ?? '?'];
        $current = $this;
        while (true) {
            if ($current->parent) {
                $current = $current->parent;
                $path[] = $current->description->name ?? '?';
            } else
                break;
        }
        return $path;
    }
}

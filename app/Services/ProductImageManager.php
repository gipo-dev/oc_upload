<?php

namespace App\Services;

use App\Product;
use App\Services\Feeds\FeedProduct;
use Illuminate\Support\Arr;

class ProductImageManager
{
    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function upload(Product $product, FeedProduct $feedProduct)
    {
        $product->update([
            'image' => $this->uploadImage($product, $feedProduct->mainImage),
        ]);

        foreach ($feedProduct->images as $image) {
            $filepath = $this->uploadImage($product, $image);
            if (!$filepath)
                continue;
            $product->images()->create([
                'image' => $filepath,
            ]);
        }
    }

    /**
     * @param $product
     * @param $filepath
     * @return string
     */
    private function uploadImage($product, $filepath)
    {
        try {
            $filename = Arr::last(explode('/', $filepath));
            $dest = config('app.yandex_disk.dest') . '/' . $product->product_id;

            if (!is_dir($dest)) {
                mkdir($dest, 0755);
            }

            rename(
                config('app.yandex_disk.source') . '/' . $this->key . '/' . $filepath,
                $dest . '/' . $filename
            );

            return 'uploads/' . $product->product_id . '/' . $filename;
        } catch (\Exception $exception) {
            return null;
        }
    }
}

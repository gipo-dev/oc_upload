<?php

namespace App\Services;

use Arhitector\Yandex\Disk;

class YandexDisk
{
    /**
     * @var mixed
     */
    private $driver;

    public function __construct()
    {
        $this->driver = app()->make(Disk::class);
    }
}

<?php

namespace App\Services;

use App\Category;
use App\Console\Commands\YandexService;
use App\Models\FeedCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryService
{
    public function getCategoriesByName($categoryNames)
    {
        $parent = null;
        $feedParent = null;

        return $categoryNames->map(function ($name) use (&$parent, &$feedParent) {
            $slug = Str::substr(Str::slug($name), -64);

            /** @var FeedCategory $feedParent */
            $feedParent = FeedCategory::with(['category'])->firstOrCreate([
                'feed' => YandexService::$folderName,
                'feed_name' => $name,
                'parent_id' => $feedParent->id ?? null,
            ]);

            if (!$feedParent->wasRecentlyCreated)
                $category = $feedParent->category;
            else
                $category = null;

            $parent = $category;
            return $category;
        });
    }
}

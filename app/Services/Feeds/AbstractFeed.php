<?php

namespace App\Services\Feeds;

use App\Product;
use App\Services\ValueParsers\AbstractValueParser;
use App\Services\ValueParsers\DefaultParser;
use App\Services\ValueParsers\ProductPropretiesParser;
use Illuminate\Support\Collection;

abstract class AbstractFeed
{
    /**
     * @var string
     */
    protected $filepath;

    /**
     * @var array
     */
    protected $keyValue;

    /**
     * @var string[]|Collection
     */
    public $headers;

    /**
     * @var Collection|string[][]
     */
    public $lines;

    /**
     * @var \App\Services\Feeds\FeedProduct[]|Collection
     */
    public $products;

    /**
     * @var bool
     */
    protected $firstOnly;

    /**
     * @var string
     */
    private $key;

    /**
     * @param string $filepath
     */
    public function __construct(string $filepath, string $key, bool $firstOnly = false)
    {
        $this->firstOnly = $firstOnly;
        $this->filepath = $filepath;
        $this->key = $key;
        $this->parseProducts();
    }

    protected function parseProducts()
    {
        $this->parseRows();
        $this->products = collect([]);

        foreach ($this->lines as $line) {
            $this->products[] = $this->parseProduct($line);
            if ($this->firstOnly)
                break;
        }
    }

    protected function parseProduct($line)
    {
        $product = new FeedProduct();

        foreach ($this->keyValue[$this->key] as $key => $conf) {
            try {
                /** @var AbstractValueParser $parser */
                if (count($conf) > 1)
                    $parser = new $conf[1]();
                else
                    $parser = new DefaultParser();
                $product->{$key} = $parser->getValue($line[$conf[0]]);
            } catch (\Exception $exception) {
//                dd($exception);
            }
        }

        try {
            $product->properties = (new ProductPropretiesParser())
                ->getValue($line, $this->headers, $product);
        } catch (\Exception $exception) {
//            dump($exception->getMessage());
        }

        return $product;
    }

    abstract protected function parseRows();

}

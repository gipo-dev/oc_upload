<?php

namespace App\Services\Feeds;


use App\Services\ValueParsers\CategoryParser;
use App\Services\ValueParsers\ImagesParser;
use App\Services\ValueParsers\InStockParser;
use App\Services\ValueParsers\InStockParser2;
use App\Services\ValueParsers\InStockParser3;
use App\Services\ValueParsers\KeyParser;

class Feed extends AbstractFeed
{
    /**
     * @var array
     */
    protected $keyValue = [
        'lqNPJBTcGl7IQg' => [
            'key' => [1, KeyParser::class],
            'name' => [4],
            'category' => [2, CategoryParser::class],
            'article' => [5],
            'inStock' => [6, InStockParser::class],
            'purchasePrice' => [7],
            'price' => [8],
            'mainImage' => [9],
            'images' => [10, ImagesParser::class],
            'description' => [11],
            'video' => [12],
        ],
        'U4FTAPgpMhaaDg' => [
            'key' => [5, KeyParser::class],
            'name' => [4],
            'category' => [3, CategoryParser::class],
            'article' => [5],
            'inStock' => [6, InStockParser::class],
            'purchasePrice' => [7],
            'price' => [8],
            'mainImage' => [9],
            'images' => [10, ImagesParser::class],
            'description' => [11],
            'video' => [12],
        ],
        'V03lrTRQkpeXgw' => [
            'key' => [5, KeyParser::class],
            'name' => [4],
            'category' => [1, CategoryParser::class],
            'article' => [5],
            'inStock' => [6, InStockParser2::class],
            'purchasePrice' => [8],
            'price' => [7],
            'mainImage' => [9],
            'images' => [10, ImagesParser::class],
            'description' => [11],
        ],
        '3NIS2zZs5sI_aw' => [
            'key' => [4, KeyParser::class],
            'name' => [3],
            'category' => [2, CategoryParser::class],
            'article' => [4],
            'inStock' => [6, InStockParser3::class],
            'purchasePrice' => [8],
            'price' => [7],
            'mainImage' => [9],
            'images' => [10, ImagesParser::class],
            'description' => [11],
            'video' => [12],
        ],
    ];

    protected function parseRows()
    {
        $result = [];
        $row = 1;
        if (($handle = fopen($this->filepath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $row++;
                $result[] = $data;
            }
            fclose($handle);
        }

        $this->headers = collect($result[0]);
        $this->lines = collect($result)->skip(1);
    }
}

<?php

namespace App\Services\Feeds;

class FeedProduct
{
    public $key;
    public $name;
    public $category;
    public $article;
    public $inStock;
    public $purchasePrice = 0;
    public $price = 0;
    public $mainImage;
    public $images;
    public $description;
    public $video;

    public $properties;

    public function __construct()
    {
        $this->properties = collect();
    }

    public function getPrice()
    {
        if (trim($this->price) != '')
            return (int)$this->price;
        return (int)$this->purchasePrice;
    }
}

<?php

namespace App\Services;

use App\Product;
use App\ProductAttribute;
use App\ProductDescription;
use App\Services\Feeds\FeedProduct;
use Illuminate\Support\Facades\DB;

class ProductGenerator
{
    private $key;

    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @param \App\Services\Feeds\FeedProduct $feedProduct
     * @return \App\Product
     */
    public function generate(FeedProduct $feedProduct)
    {
        if (!$feedProduct->category || !$feedProduct->category->last()) {
            dump('Не задана категория ' . $feedProduct->name);
            return;
        }

//        Product::where('import_key', $feedProduct->key)->delete();
        /** @var Product $product */
        $product = Product::firstOrNew([
            'import_key' => $feedProduct->key,
            'import_batch' => 1,
        ], [
            'model' => $feedProduct->article ?? $feedProduct->key,
            'date_available' => now(),
            'date_added' => now(),
            'date_modified' => now(),
        ]);

        $product->fill([
            'stock_status_id' => $feedProduct->inStock ? 7 : 5,
            'price' => $feedProduct->getPrice(),
        ]);
        $product->save();

        dump($product->product_id);

        if ($product->wasRecentlyCreated) {
            $product->description()->create([
                'name' => $feedProduct->name,
                'description' => $feedProduct->description,
                'meta_h1' => $feedProduct->name,
            ]);

            try {
                DB::table('oc_product_to_category')->insert([
                    'product_id' => $product->product_id,
                    'category_id' => $feedProduct->category->where('parent_id', '!=', null)->last()->category_id,
                    'main_category' => 1,
                ]);
            } catch (\Exception $exception) {
            }

            try {
                DB::table('oc_product_to_store')->insert([
                    'product_id' => $product->product_id,
                    'store_id' => 0,
                ]);
            } catch (\Exception $exception) {
            }

            $feedProduct->properties->each(function ($property) use ($product) {
                $pa = ProductAttribute::where('product_id', $product->product_id)
                    ->where('attribute_id', $property['id'])->first();

                if (!$pa) {
                    $pa = new ProductAttribute();
                    DB::table($pa->getTable())->insert(array_merge(
                        $pa->getAttributes(),
                        [
                            'product_id' => $product->product_id,
                            'attribute_id' => $property['id'],
                            'text' => $property['val'],
                        ]
                    ));
                } else {
                    $pa->save([
                        'text' => $property['val'],
                    ]);
                }
            });

            (new ProductImageManager($this->key))->upload($product, $feedProduct);
        }

        return $product;
    }
}

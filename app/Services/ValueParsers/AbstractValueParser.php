<?php

namespace App\Services\ValueParsers;

abstract class AbstractValueParser
{
    /**
     * @param $data
     * @return mixed
     */
    abstract public function getValue($data);
}

<?php

namespace App\Services\ValueParsers;

class InStockParser extends AbstractValueParser
{
    public function getValue($data)
    {
        return $data == 'В наличии';
    }
}

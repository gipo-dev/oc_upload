<?php

namespace App\Services\ValueParsers;

use Illuminate\Support\Str;

class KeyParser extends AbstractValueParser
{
    public function getValue($data)
    {
        return Str::slug($data);
    }
}

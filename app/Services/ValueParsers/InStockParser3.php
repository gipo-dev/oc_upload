<?php

namespace App\Services\ValueParsers;

class InStockParser3 extends AbstractValueParser
{
    public function getValue($data)
    {
        return $data == 'есть в наличии';
    }

}

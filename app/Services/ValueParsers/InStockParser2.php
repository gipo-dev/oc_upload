<?php

namespace App\Services\ValueParsers;

class InStockParser2 extends AbstractValueParser
{
    public function getValue($data)
    {
        return $data != 'Отсутствует на складе' && $data != 0;
    }

}

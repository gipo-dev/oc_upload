<?php

namespace App\Services\ValueParsers;

use App\Attribute;
use App\AttributeDescription;
use App\AttributeGroup;
use App\AttributeGroupDescription;
use App\ProductAttribute;

class ProductPropretiesParser
{
    public function getValue($data, $headers, $product)
    {
        $data = collect($data)->skip(13);
        $headers = $headers->skip(13);

        return $data->map(function ($value, $key) use ($headers, $product) {
            try {
                $name = $headers[$key];
                return $this->getAttribute($name, $value, $product);
            } catch (\Exception $exception) {
                return null;
            }
        })->filter();

    }

    /**
     * @param $name
     * @param $value
     * @param $product
     * @return mixed
     */
    private function getAttribute($name, $value, $product)
    {

        $ad = AttributeDescription::where('name', $name)->first();

        if (!$ad) {
            $ag = $this->getAttributeGroup($name, $value, $product);

            /** @var \App\Attribute $attr */
            $attr = $ag->attrs()->create();

            $attr->description()->create([
                'name' => $name,
            ]);
        } else
            $attr = Attribute::find($ad->attribute_id);

        return [
            'id' => $attr->attribute_id,
            'val' => $value,
        ];
    }

    /**
     * @param $name
     * @param $value
     * @param $product
     * @return AttributeGroup
     */
    private function getAttributeGroup($name, $value, $product)
    {
        $categoryName = $product->category->where('parent_id', '!=', null)->last()->description->name ?? 'Другое';
        $agd = AttributeGroupDescription::where('name', $categoryName)->first();

        /** @var AttributeGroup $ag */
        if (!$agd) {
            $ag = AttributeGroup::create();
            $ag->description()->create([
                'name' => $categoryName,
            ]);
        } else
            $ag = AttributeGroup::find($agd->attribute_group_id);

        return $ag;
    }
}

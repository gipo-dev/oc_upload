<?php

namespace App\Services\ValueParsers;

use App\Category;
use App\Services\CategoryService;
use Illuminate\Support\Str;

class CategoryParser extends AbstractValueParser
{
    public function getValue($data)
    {
        $categories = collect(explode('|', $data));
        return (new CategoryService())->getCategoriesByName($categories);
    }
}

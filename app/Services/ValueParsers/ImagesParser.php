<?php

namespace App\Services\ValueParsers;

class ImagesParser extends AbstractValueParser
{
    public function getValue($data)
    {
        return explode('|', $data);
    }
}

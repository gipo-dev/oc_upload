<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeDescription extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_attribute_description';

    /**
     * @var string
     */
    protected $primaryKey = 'attribute_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'language_id' => 1,
    ];
}

<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Models\FeedCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.categories', [
            'feedCategories' => FeedCategory::with(['category', 'category.description'])->get()->groupBy('feed')
                ->map(function ($feed) {
                    return $feed->groupBy('parent_id');
                }),
            'localCategories' => Category::with(['description', 'parent', 'parent.description'])
                ->whereDoesntHave('childs')->get(),
            'listCategories' => Category::with(['description'])
                ->get()->groupBy('parent_id'),
        ]);
    }

    public function set(Request $request)
    {
        FeedCategory::find($request->id)->update([
            'category_id' => $request->value,
        ]);
    }

    public function remove(Request $request)
    {
        FeedCategory::find($request->id)->update([
            'category_id' => null,
        ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryDescription extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_category_description';


    /**
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $attributes = [
        'language_id' => 1,
        'description' => '',
        'meta_title' => '',
        'meta_description' => '',
        'meta_keyword' => '',
        'meta_h1' => '',
    ];
}

<?php

namespace App\Console\Commands;

use App\Services\Feeds\Feed;
use App\Services\Feeds\FeedProduct;
use App\Services\ProductGenerator;
use Arhitector\Yandex\Client\OAuth;
use Arhitector\Yandex\Disk;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class YandexService extends Command
{
    /**
     * @var mixed
     */
    public static $folderName;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'feed:parse {--first}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Disk $disk, ProductGenerator $generator)
    {

        collect($disk->getPublishResource('https://disk.yandex.ru/d/VDzBmPXzD8dx9g')->toArray()['items'])
            ->pluck('public_url')
//            ->skip(3)
            ->filter()->each(function ($url) use (&$disk, &$generator) {
                $res = $this->getResource($url, $disk);
                $folderName = Arr::last(explode('/', $url));
                static::$folderName = $folderName;
                dump($folderName);


                // Изображения
                $this->getResource($res->where('name', 'files')->first()->public_url, $disk)
                    ->each(function ($item) use ($folderName) {
                        $folderName = storage_path('result/' . $folderName . '/files');
                        $fileName = $folderName . '/' . $item->name;

                        if (!is_dir($folderName)) {
                            mkdir($folderName, 0755);
                        }

                        if (file_exists($fileName))
                            return;

                        $file = file_get_contents($item->file);
                        file_put_contents($fileName, $file);
                    });

                // Фиды
                $res->filter(function ($item) {
                    return Str::endsWith($item->name, '.csv');
                })->each(function ($feed) use ($folderName, &$generator) {

                    if (!is_dir(storage_path('result'))) {
                        mkdir(storage_path('result'), 0755);
                    }

                    $key = $folderName;
                    $folderName = storage_path('result/' . $folderName);
                    if (!is_dir($folderName)) {
                        mkdir($folderName, 0755);
                    }

                    $file = file_get_contents($feed->file);
                    file_put_contents($folderName . '/' . $feed->name, $file);

                    // Генерация товаров
                    $feed = new Feed(
                        $folderName . '/' . $feed->name,
                        $key,
                        $this->option('first' ?? false)
                    );

                    $feed->products->each(function (FeedProduct $feedProduct) use (&$generator) {
                        $this->info($feedProduct->name);
                        if (!$feedProduct->key)
                            return;

                        // генерация и сохранение товара
                        try {
                            $generator->generate($feedProduct);
                        } catch (\Exception $exception) {
//                            dd($exception, $feedProduct);
                            $this->error($exception->getMessage());
                        }
                    });

                });

            });
    }

    /**
     * @param $url
     * @param $disk
     * @return \Illuminate\Support\Collection
     */
    private function getResource($url, $disk)
    {
        return collect($disk->getPublishResource($url, 50000)->toArray()['items']);
    }

    /**
     * Define the command's schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

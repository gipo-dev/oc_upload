<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'oc_product_description';

    /**
     * @var string
     */
    protected $primaryKey = 'product_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $attributes = [
        'language_id' => 1,
        'tag' => '',
        'meta_title' => '',
        'meta_description' => '',
        'meta_keyword' => '',
    ];
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => config('app.prefix_folder')], function () {

    Route::get('/', function () {
        return redirect()->route('admin.index');
    });

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('main', function () {
            return redirect()->route('admin.categories.index');
        })->name('index');

        Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {

            Route::get('/', [\App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('index');
            Route::get('set', [\App\Http\Controllers\Admin\CategoryController::class, 'set'])->name('set');
            Route::get('remove', [\App\Http\Controllers\Admin\CategoryController::class, 'remove'])->name('remove');

        });
    });

});
